require('dotenv').config({
  path: './env/.env.development',
});

const app = require('express')();
const http = require('http');

const config = require('./config');
const consumer = require('./services/consumer');

const server = http.createServer(app);
const port = config.PORT;
const queue = 'worker';

server.listen(port, async () => {
  await consumer.connect();
  await consumer.consumeFromQueue(queue);
  console.log(`Server started on port ${port}`);
});
