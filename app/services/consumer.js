const amqplib = require('amqplib-as-promised');
const config = require('../config');

const connectionString = `amqp://${config.RABBITMQ_USER}:${config.RABBITMQ_PASSWORD}@${config.RABBITMQ_HOST}`;

const connection = new amqplib.Connection(connectionString);
let channel;

const connect = async () => {
  await connection.init();
  channel = await connection.createChannel();
};

const consumeFromQueue = async (queue) => {
  channel.consume(queue, async (payload) => {
    console.log(JSON.parse(payload.content.toString()));

    // Send acknowledgement to RabbitMQ to remove msg from queue.
    await channel.ack(payload);
  });
};

const disconnect = async () => {
  await channel.close();
  await connection.close();
};

module.exports = {
  connect,
  consumeFromQueue,
  disconnect,
};
